import { Component } from '@angular/core';

@Component({
  selector: 'loader-report-chunk',
  templateUrl: './loader.report-chunk.html',
  styleUrls: ['./loader.report-chunk.scss'],
})
export class LoaderReportChunk {}
