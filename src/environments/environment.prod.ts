import { AppEnvironment } from '@environments/environment.class';
import { StorageDrivers } from '@sumaris-net/ngx-components';

/* eslint-disable */
const pkg = require('../../package.json');
export const environment = Object.freeze(<AppEnvironment>{
  name: pkg.name as string,
  version: pkg.version as string,
  production: true,
  baseUrl: '/',
  useHash: false,
  defaultLocale: 'fr',
  defaultLatLongFormat: 'DDMM',
  apolloFetchPolicy: 'cache-first',
  allowDarkMode: true,

  // Environment
  externalEnvironmentUrl: 'assets/environments/environment.json',
  // Must be change manually. Can be override using Pod properties 'sumaris.app.min.version'
  peerMinVersion: '2.9.27',

  // Enable selection by programs
  enableSelectPeerByFeature: true,

  // Check Web new app version
  checkAppVersionIntervalInSeconds: 5 * 60, // every 5min

  // FIXME: enable cache
  persistCache: false,

  // Leave null,
  defaultPeer: null,

  defaultPeers: [
    // -- Production and public peers --
    {
      host: 'www.sumaris.net',
      port: 443,
      useSsl: true,
    },
    {
      host: 'open.sumaris.net',
      port: 443,
      useSsl: true,
    },
    {
      host: 'adap.pecheursdebretagne.eu',
      port: 443,
      useSsl: true,
    },
    {
      host: 'imagine-pod.ifremer.fr',
      port: 443,
      useSsl: true,
    },
    {
      host: 'sih.sfa.sc',
      port: 443,
      useSsl: true,
    },
    {
      host: 'opus-activite-pod.ifremer.fr',
      port: 443,
      useSsl: true,
    },

    // -- Tests instances --
    // FIXME - disabled for now § until we a have a flag on test DB API
    /*{
      host: 'adap-test.pecheursdebretagne.eu',
      port: 443,
      useSsl: true,
    },
    {
      host: 'open-test.sumaris.net',
      port: 443,
      useSsl: true,
    },
    {
      host: 'test.sumaris.net',
      port: 443,
      useSsl: true,
    },
    {
      host: 'obsmer.sumaris.net',
      port: 443,
      useSsl: true,
    },*/
  ],

  defaultAppName: 'SUMARiS',
  defaultAndroidInstallUrl: 'https://play.google.com/store/apps/details?id=net.sumaris.app',
  defaultIOSInstallUrl: 'https://apps.apple.com/app/id6736747523',
  defaultDesktopInstallUrl: 'https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/releases',

  // About modal
  sourceUrl: 'https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app',
  reportIssueUrl: 'https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-app/-/issues/new?issue',
  forumUrl: null, // 'https://forum.sumaris.net',
  //helpUrl: 'https://gitlab.ifremer.fr/sih-public/sumaris/sumaris-doc/-/blob/master/user-manual/index_fr.md',
  privacyPolicyUrl: '/assets/doc/privacy_policy.md',
  termsOfUseUrl: '/assets/doc/terms_of_use.md',

  // Storage
  storage: {
    driverOrder: [StorageDrivers.SQLLite, StorageDrivers.IndexedDB, StorageDrivers.WebSQL, StorageDrivers.LocalStorage],
  },

  account: {
    enableListenChanges: true,
    listenIntervalInSeconds: 0,
  },

  entityEditor: {
    enableListenChanges: true,
    listenIntervalInSeconds: 0,
  },

  program: {
    enableListenChanges: true,
    listenIntervalInSeconds: 30,
  },

  menu: {
    subMenu: {
      enable: true,
    },
  },
});
/* tslint:enable */
