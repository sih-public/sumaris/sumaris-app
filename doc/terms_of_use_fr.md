# Conditions Générales d'Utilisation (CGU)

> Dernière mise à jour : **7 janvier 2025**
>
> [English version >>](./terms_of_use.md)
 

## 1. Objet des CGU
La présente application permet la saisie, la validation et la gestion de données halieutiques, dans le cadre de programmes de collecte. Les utilisateurs, organismes et responsables de programme s’engagent à respecter les règles d’utilisation décrites dans ce document.

---

## 2. Définitions
- **Programme de collecte** : Ensemble structuré définissant les protocoles de saisie, les droits d’accès et les utilisateurs habilités, sous la responsabilité d’un ou plusieurs responsables de programme.
- **Utilisateur** : Toute personne disposant d’un accès à l’application, avec des droits définis par son rôle (saisisseur, superviseur, ou responsable de programme).
- **Superviseur** : Utilisateur ayant la capacité de superviser des saisies, valider ou dévalider des données d’un organisme.
- **Responsable de programme** : Gestionnaire principal d’un programme de collecte, chargé de l’accès, de la gestion et du règlement des litiges.
- **Administrateur de la plateforme** : La société opérateur technique et hébergeur de l’application.

---

## 3. Saisie et gestion des données
1. **Accès aux programmes de collecte** :
  - Seuls les utilisateurs disposant de droits spécifiques peuvent accéder aux données d’un programme.
  - La liste des programmes, sans données associées, reste publique et accessible à tous.

2. **Saisie de données** :
  - Un utilisateur peut saisir une donnée dans un programme uniquement s’il dispose des droits nécessaires.
  - Chaque donnée saisie est rattachée à un programme de collecte et aux champs définis par ce programme.

3. **Modification des données** :
  - Les utilisateurs peuvent modifier leurs données tant qu’elles ne sont pas validées.
  - Une fois une donnée validée, elle ne peut plus être ni supprimée ni modifiée.

4. **Supervision des données** :
  - Les superviseurs peuvent accéder aux données des utilisateurs qu’ils supervisent. Ils ont la capacité de valider ou dévalider des données au sein de l’organisme concerné.

5. **Extraction des données** :
  - Les données validées peuvent être exportées au format CSV :
    - **Les saisisseurs** peuvent extraire leurs propres données.
    - **Les superviseurs** peuvent extraire les données validées des utilisateurs qu’ils supervisent.
    - **Les responsables de programme** peuvent extraire les données validées de tout le programme.
  - Certains formats d’export imposent une anonymisation obligatoire pour certains champs ; d’autres formats permettent une anonymisation optionnelle.

---

## 4. Suppression des comptes utilisateurs
1. Tout utilisateur peut demander la suppression de son compte en utilisant le formulaire suivant : [https://forms.gle/ih3PyEioek7VQcTj7](https://forms.gle/ih3PyEioek7VQcTj7).
2. La suppression sera effective dans un délai maximum de **30 jours** après réception de la demande.
3. Les données validées saisies par l’utilisateur resteront cependant liées au programme de collecte concerné et à ses responsables, conformément aux protocoles définis.

---

## 5. Propriété des données
1. Les données validées saisies dans l’application sont la propriété exclusive des programmes de collecte et des responsables d’organismes associés.
2. Les utilisateurs ne disposent d’aucun droit de propriété sur ces données, mais conservent un droit d’accès selon les protocoles définis.

---

## 6. Gestion des litiges
1. Les éventuels litiges en lien avec l’utilisation de l’application doivent être déclarés auprès des administrateurs de la plateforme.
2. Une commission composée des responsables de programmes actifs hébergés sur la plateforme émettra un avis consultatif.
3. La décision finale revient au responsable du programme de collecte concerné.

---

## 7. Limitation de responsabilité
La société hébergeuse et opératrice de l’application, n’assume aucune responsabilité dans les cas suivants :
- Bugs ou erreurs techniques affectant l’application.
- Perte ou corruption de données due à un problème matériel sur l’appareil de l’utilisateur.
- Problèmes de connectivité réseau.
- Mauvaise manipulation de l’utilisateur, comme la suppression accidentelle d’une donnée.
- Toute autre utilisation non conforme des outils de saisie, de validation, de modification ou d'extraction.

---

## 8. Modifications des CGU
Les présentes CGU peuvent être modifiées à tout moment par la société hébergeuse et opératrice de l’application. Toute nouvelle version sera publiée sur l’application avec un préavis raisonnable et communiqué aux utilisateurs si nécessaire.

---

**Contact pour toute information supplémentaire :**  

Société **Environmental Information Systems** : **contact@e-is.pro** - 19 rue Cassiopée, 53470 Martigné-Sur-Mayenne, FRANCE
