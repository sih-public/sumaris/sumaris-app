# General Terms of Use (ToU)

> Last updated: **January 7, 2025**
>
> [Version française >>](./terms_of_use_fr.md)

## 1. Purpose of the ToU
This application allows the entry, validation, and management of fisheries-related data within the context of collection programs. Users, organizations, and program managers agree to comply with the usage rules described in this document.

---

## 2. Definitions
- **Collection program**: A structured framework defining entry protocols, access rights, and authorized users, under the responsibility of one or more program managers.
- **User**: Any person with access to the application, with rights defined by their role (data entry operator, supervisor, or program manager).
- **Supervisor**: A user with the ability to oversee entries, validate or invalidate data from an organization.
- **Program manager**: The main administrator of a collection program, responsible for access, management, and dispute resolution.
- **Platform administrator**: The company acting as technical operator and host of the application.

---

## 3. Data entry and management
1. **Access to collection programs**:
- Only users with specific rights may access program data.
- The list of programs, without associated data, remains public and accessible to everyone.

2. **Data entry**:
- A user may enter data within a program only if they have the required rights.
- Each entered data is linked to a relevant collection program and the fields defined by that program.
open
3. **Data modification**:
- Users may modify their data as long as it hasn’t been validated.
- Once validated, data can no longer be deleted or modified.

4. **Data supervision**:
- Supervisors may access the data of the users they oversee. They have the ability to validate or invalidate data within the concerned organization.

5. **Data extraction**:
- Validated data can be exported in CSV format:
  - **Data entry operators** can extract their own data.
  - **Supervisors** can extract validated data from the users they oversee.
  - **Program managers** can export validated data from the entire program.
- Certain export formats require mandatory anonymization for specific fields, while other formats allow optional anonymization.

---

## 4. Deletion of user accounts
1. Any user may request the deletion of their account by using the following form: [https://forms.gle/ih3PyEioek7VQcTj7](https://forms.gle/ih3PyEioek7VQcTj7).
2. The deletion will take effect within a maximum of **30 days** upon receiving the request.
3. Validated data submitted by the user will remain linked to the concerned collection program and its managers, according to the defined protocols.

---

## 5. Data ownership
1. Validated data submitted within the application is the exclusive property of the collection programs and associated organization managers.
2. Users do not hold any ownership rights over this data but retain access rights as defined by the protocols.

---

## 6. Dispute resolution
1. Any disputes related to the use of the application must be reported to the platform administrators.
2. A commission composed of managers of active programs hosted on the platform will issue an advisory opinion.
3. The final decision lies with the manager of the concerned collection program.

---

## 7. Limitation of liability
The company hosting and operating the application assumes no responsibility in the following cases:
- Bugs or technical errors affecting the application.
- Loss or corruption of data due to hardware issues on the user’s device.
- Network connectivity issues.
- User mishandling, such as accidental deletion of data.
- Any other misuse of data entry, validation, modification, or extraction tools.

---

## 8. Modifications to the ToU
The present ToU may be modified at any time by the company hosting and operating the application. Any new version will be published on the application with reasonable notice and communicated to users if necessary.

---

**Contact for additional information:**  

Company **Environmental Information Systems** : **contact@e-is.pro** - 19 rue Cassiopée, 53470 Martigné-Sur-Mayenne, FRANCE
